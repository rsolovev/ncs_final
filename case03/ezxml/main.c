#include <ezxml.h>

int main() {
    printf("Reading normal xml file ...\n");
    ezxml_t norm = ezxml_parse_file("example.xml");
    printf("Chech xml name: %s\n", norm->name);
    printf("Reading broken xml file ...\n");
    ezxml_t crash = ezxml_parse_file("crash.xml");
    // This code is not reachable
    printf("Check xml name: %s\n", crash->name);
    return 0;
}
