More detailed info about this case is available in the report

exploit steps:
1. run particular H2 version from dockerhub using following command:
sudo docker run -d -p 1521:1521 -p 81:81 --name=MyH2Instance oscarfonts/h2:1.4.197

2. wait for container to start and save the container id somewhere

3. run db population script using command:
python3 h2_populate_db.py

4. navigate to localhost:81 in your browser and click 'connect'

5. verify that table is created by typing in the command window in console
SELECT * from test;
and click 'run' in left upper corner

6. see that content of 'test' database is shown with some random info

7. leave the console and run the exploit:
python3 h2_exploit.py

8. script will report ubnormal connection abortion, and that is normal, as firstly it connects to the database,
creates alias for shell comnmands and then brings down the container, causing connection abortion.

9. you can check that the container is actually dead:
sudo docker container ls

10. if you saved the hash number (id) of container (printed on the start), you can restart it using
sudo docker container restart CONTAINER_ID

11. now you can verify that all the data was wiped from the container memory by visiting localhost:81 again
and checking that no table is available
