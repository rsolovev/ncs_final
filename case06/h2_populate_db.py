import html

import requests


def getCookie(host):
    url = 'http://{}'.format(host)
    r = requests.get(url)
    path = r.text.split('href = ')[1].split(';')[0].replace("'", "").replace('.jsp', '.do')
    return '{}/{}'.format(url, path)


def login(url, user, passwd, database):
    data = {'language': 'en', 'setting': 'Generic+H2+(Embedded)', 'name': 'Generic+H2+(Embedded)',
            'driver': 'org.h2.Driver', 'url': database, 'user': user, 'password': passwd}
    r = requests.post(url, data=data)
    if '<th class="login">Login</th>' in r.text:
        return False
    return True

host = '127.0.0.1:81'
user = 'sa'
password = ''
database_url = 'jdbc:h2:tcp://localhost:1521/test'

url = getCookie(host)
print(url)
if login(url, user, password, database_url):
    cmd = '''DROP TABLE IF EXISTS TEST;CREATE TABLE TEST(ID INT PRIMARY KEY,NAME VARCHAR(255));INSERT INTO TEST VALUES(1, 'Hellso1');INSERT INTO TEST VALUES(2, 'Wodrld1');'''
    url = url.replace('login', 'query')
    r = requests.post(url, data={'sql': cmd})
print("Success")
